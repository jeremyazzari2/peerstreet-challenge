# API and Client for finding Metropolatian Statistical Area by Zip Code.

## Api

### Tech
I used the serverless framework https://serverless.com/ to build the api using node.js and postgres. The reason for using serverless is mostly that I always wanted to build something using it:) It also forces you to deploy early and often, which I am a fan of.

Deploy to any AWS account by installing serverless, setting up your credentials via serverless docs and running `serverless deploy`.

api host:
`https://0cwrmyg9ub.execute-api.us-west-1.amazonaws.com/dev`
there is only one endpoint:
`GET /msa?zip=[zip]`

example request
```
curl https://0cwrmyg9ub.execute-api.us-west-1.amazonaws.com/dev/msa\?zip\=00501
```
The response format is:
```
{
  "zip":"00501",
  "cbsa":"35620",
  "name":"New York-Newark-Jersey City, NY-NJ-PA",
  "population":{
    "2014": "20095119",
    "2015": "20182305"
  }
}
```

The bulk of the interesting code is in `api/src`

### Considerations

I created 2 tables using the import files using only the columns I needed, except for the POPESTIMATE columns which I included all combined in an `hstore` column called population_estimates.
You can set which years to return by using a comma separated string of years as a value for the
`POPULATION_YEARS=2013,2014,2015` environment variable. Any column with the format `POPESTIMATEXXXX` will be extracted into the `hstore` on import so it should be really easy to update with new data.

The main query the api uses is:
```javascript
  // api/src/find_msa.js
  const sql = `
    SELECT cbsa, name, population_estimates FROM ${MSA_TABLE_NAME}
    WHERE lsad = 'Metropolitan Statistical Area'
    AND cbsa IN (
      SELECT COALESCE(m.cbsa, z.cbsa) AS cbsa FROM ${ZIP_TABLE_NAME} AS z
      LEFT JOIN ${MSA_TABLE_NAME} AS m ON z.cbsa = m.mdiv
      WHERE zip = $1
    );
  `
```

I created several npm scripts for importing data.
Usage  `npm {script-name}`
```
  "test": "mocha",
  "initialize-db": "node -e 'require(\"./src/migrate_db.js\").up()",
  "drop-tables": "node -e 'require(\"./src/migrate_db.js\").down()",
  "import-msa": "node -e 'require(\"./src/import_msa.js\").importCSV()",
  "import-zip": "node -e 'require(\"./src/import_zip.js\").importCSV()"
```

import-msa and import-zip use the respective environment variables to find the import file.
```
MSA_IMPORT_PATH=./data/cbsa_to_msa.csv
ZIP_IMPORT_PATH=./data/zip_to_cbsa.csv
```

## Client

### Tech

I created a Ruby gem around a very simple api client.

gems used:
- rspec
- httparty
- webmock
- pry

The easiest way to try it out is using the console.
Assuming you have bundler set up.

```
cd client/zip_to_msa
bundle install
bundle exec bin/console
```
```
#pry>
client = ZipToMsa::Client.new()
client.find_by_zip('03061')
# => {:zip=>"03061", :cbsa=>"31700", :name=>"Manchester-Nashua, NH", :population=>{"2014"=>"405339", "2015"=>"406678"}}
```

run tests
```
bundle exec rake test
```