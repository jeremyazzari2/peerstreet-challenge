require 'spec_helper'

describe ZipToMsa::Client do
  subject { ZipToMsa::Client.new() }

  base_uri = "https://0cwrmyg9ub.execute-api.us-west-1.amazonaws.com/dev"

  describe '#find_by_zip' do
    it 'properly formats the response' do
      stub_request(:get, "#{base_uri}/msa?zip=00501").
      to_return(
        headers: {content_type: 'application/json'}, 
        body: {"zip":"00501","cbsa":"35620","name":"New York-Newark-Jersey City, NY-NJ-PA","population":{"2014":"20095119","2015":"20182305"}}.to_json
      )

      response = {
        zip: '00501',
        cbsa:  '35620', 
        name:  'New York-Newark-Jersey City, NY-NJ-PA', 
        population:  {
          '2014' => '20095119', 
          '2015' => '20182305'
        }
      }

      expect(subject.find_by_zip('00501')).to eq(response)
    end

    it 'properly formats the response when no msa' do
      stub_request(:get, "#{base_uri}/msa?zip=00501").
      to_return(
        headers: {content_type: 'application/json'}, 
        body: {"zip":"00501"}.to_json
      )

      response = {
        zip: '00501'
      }

      expect(subject.find_by_zip('00501')).to eq(response)
    end


    it 'properly formats the response when error' do
      stub_request(:get, "#{base_uri}/msa?zip=").
      to_return(
        headers: { content_type: 'application/json' }, 
        body: { error: "missing query parameter" }.to_json
      )

      response = {
        error: "missing query parameter"
      }

      expect(subject.find_by_zip(nil)).to eq(response)
    end

    it 'raises error' do
      expect { subject.find_by_zip }.to raise_error ArgumentError
    end
  end
end