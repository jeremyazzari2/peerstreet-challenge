require "zip_to_msa/version"
require 'httparty'

module ZipToMsa
  class Client
    include HTTParty

    base_uri "https://0cwrmyg9ub.execute-api.us-west-1.amazonaws.com/dev"

    def parse_response(response)
      response.parsed_response
      response = response.inject({}){|memo,(k,v)| memo[k.to_sym] = v; memo}
    end

    def find_by_zip(zip)
      options = {
        query: { zip: zip }
      }
      parse_response self.class.get('/msa', options)
    end
  end
end
