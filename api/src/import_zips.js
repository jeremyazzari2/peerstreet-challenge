'use strict'

require('dotenv').config()

const fs = require('fs')
const { Client } = require('pg')
const { parseCSV } = require('./parse_csv')

const ZIP_TABLE_NAME = process.env.ZIP_TABLE_NAME

function insert (rows, client) {
  return new Promise((resolve, reject) => {
    const sql = `INSERT into ${ZIP_TABLE_NAME} (zip, cbsa) VALUES ($1, $2)`
    try {
      rows.forEach(async function (values, index) {
        await client.query(sql, values)
        if (index === rows.length - 1) resolve()
      })
    } catch (e) {
      reject(e)
    }
  })
}

async function importCSV (fileStream) {
  const stream = fileStream || fs.createReadStream(process.env.ZIP_IMPORT_PATH)

  // Read CSV rows.
  const rows = await parseCSV(stream)
  rows.shift()// remove headers.

  const mappedRows = rows.map(row => [`${row.ZIP.padStart(5, 0)}`, `${row.CBSA}`])

  // Insert into database.
  const cl = new Client()
  await cl.connect()
  await insert(mappedRows, cl)
  await cl.end()
}

module.exports = { importCSV }
