'use strict'

require('dotenv').config()

const hstore = require('pg-hstore')()
const fs = require('fs')
const { Client } = require('pg')
const { parseCSV } = require('./parse_csv')

const MSA_TABLE_NAME = process.env.MSA_TABLE_NAME

function insert (rows, client) {
  return new Promise((resolve, reject) => {
    const sql = `INSERT into ${MSA_TABLE_NAME} (cbsa, mdiv, name, lsad, population_estimates) VALUES ($1, $2, $3, $4, $5)`
    try {
      rows.forEach(async function (values, index) {
        await client.query(sql, values)
        if (index === rows.length - 1) resolve()
      })
    } catch (e) {
      reject(e)
    }
  })
}

async function importCSV (fileStream = null) {
  const stream = fileStream || fs.createReadStream(process.env.MSA_IMPORT_PATH)

  // Read CSV rows.
  const rows = await parseCSV(stream)
  rows.shift() // remove headers.

  // Format rows for sql storage.
  const mappedRows = rows.filter(row => row.CBSA).map((row) => {
    // Map populations for hstore.
    const populations = Object.keys(row).reduce((acc, key) => {
      if (key.match(/^POPESTIMATE/)) {
        acc[key] = row[key]
      }
      return acc
    }, {})

    let serializedPopulation
    hstore.stringify(populations, function (result) {
      serializedPopulation = result
    })

    return [
      row.CBSA,
      row.MDIV,
      row.NAME,
      row.LSAD,
      serializedPopulation
    ]
  })

  // Insert into database.
  const cl = new Client()
  await cl.connect()
  await insert(mappedRows, cl)
  await cl.end()
}

module.exports = { importCSV }
