'use strict'

require('dotenv').config()
const { Client } = require('pg')

const createZips = `CREATE TABLE zips (
  id serial PRIMARY KEY NOT NULL,
  zip char(16) NOT NULL,
  cbsa CHAR(32)
)`

const createMsa = `CREATE TABLE msa (
  id serial PRIMARY KEY NOT NULL,
  cbsa CHAR(32),
  mdiv CHAR(32),
  name CHAR(255),
  lsad CHAR(255),
  population_estimates hstore
)`

async function up () {
  const client = new Client()
  await client.connect()

  await client.query(createZips)
  await client.query(createMsa)

  await client.end()
}

async function down () {
  const client = new Client()
  await client.connect()

  await client.query(`DROP TABLE zips`)
  await client.query(`DROP TABLE msa`)

  await client.end()
}

module.exports = { up, down }
