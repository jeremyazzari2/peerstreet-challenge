'use strict'

require('dotenv').config()

var hstore = require('pg-hstore')()

const { Client } = require('pg')

const MSA_TABLE_NAME = process.env.MSA_TABLE_NAME
const ZIP_TABLE_NAME = process.env.ZIP_TABLE_NAME
const POPULATION_YEARS = process.env.POPULATION_YEARS || '2014,2015'
const populationYearsArr = POPULATION_YEARS.split(',')

const sql = `
  SELECT cbsa, name, population_estimates FROM ${MSA_TABLE_NAME}
  WHERE lsad = 'Metropolitan Statistical Area'
  AND cbsa IN (
    SELECT COALESCE(m.cbsa, z.cbsa) AS cbsa FROM ${ZIP_TABLE_NAME} AS z
    LEFT JOIN ${MSA_TABLE_NAME} AS m ON z.cbsa = m.mdiv
    WHERE zip = $1
  );
`

module.exports.findMsaByZip = async function (zip) {
  if (!zip) return { error: 'missing query parameter' }
  if (!zip.trim().match(/^(\d{5})$/)) return { error: 'invalid zipcode!' }

  const client = new Client()
  await client.connect()
  const result = await client.query(sql, [zip])

  await client.end()

  const formattedRows = result.rows.reduce((acc, row) => {
    let population
    hstore.parse(row.population_estimates, function (result) {
      population = result
    })

    const filteredPopulation = Object.keys(population).reduce((acc, key) => {
      const year = key.slice(-4)

      if (populationYearsArr.includes(year)) {
        acc[year] = population[key]
      }
      return acc
    }, {})

    acc.push({
      cbsa: row.cbsa.trim(),
      name: row.name.trim(),
      population: filteredPopulation
    })
    return acc
  }, [])

  return { zip, ...formattedRows[0] }
}
