'use strict'

const csv = require('fast-csv')
const fs = require('fs')
const sutil = require('line-stream-util')

// Parse the csv.
const parse = (stream, resolve, rows) => (csvHeaders) => {
  csv
    .fromStream(stream, {
      headers: csvHeaders.trim().split(',')
    })
    .on('data', function (data) {
      if (data[csvHeaders[0]] !== csvHeaders[0]) {
        rows.push(data)
      }
    })
    .on('end', function (data) {
      resolve(rows)
    })
}

module.exports.parseCSV = function (fileStream) {
  let rows = []

  return new Promise((resolve, reject) => {
    try {
      // copy the file stream since we mess up the original to get the headers
      const headerStream = fs.createReadStream(fileStream.path)

      // Get the headers from the first line.
      fileStream
        .pipe(sutil.head(1))
        .pipe(sutil.split())
        .setEncoding('utf8')
        .on('data', parse(headerStream, resolve, rows))
    } catch (e) {
      reject(e)
    }
  })
}
