var assert = require('assert')
var { findMsaByZip } = require('../src/find_msa.js')

describe('Array', function () {
  describe('#findMsaByZip()', function () {
    it('has z.cbsa and m.mdiv match', async function () {
      const result = await findMsaByZip('00501')
      assert.equal(
        JSON.stringify(result), JSON.stringify({
          zip: '00501',
          cbsa: '35620',
          name: 'New York-Newark-Jersey City, NY-NJ-PA',
          population: {
            2014: '20095119',
            2015: '20182305'
          }
        })
      )
    })

    it('has zip.cbsa and m.cbsa match, no mdiv match', async function () {
      const result = await findMsaByZip('01001')
      assert.equal(
        JSON.stringify(result), JSON.stringify({
          zip: '01001',
          cbsa: '44140',
          name: 'Springfield, MA',
          population: {
            2014: '630672',
            2015: '631982'
          }
        })
      )
    })

    it('has no msa match', async function () {
      const result = await findMsaByZip('00604')
      assert.equal(
        JSON.stringify(result), JSON.stringify({
          zip: '00604'
        })
      )
    })

    it('is missing zipcode', async function () {
      const result = await findMsaByZip()
      assert.equal(
        JSON.stringify(result), JSON.stringify({
          error: 'missing query parameter'
        })
      )
    })

    it('is an invalid zipcode', async function () {
      const result = await findMsaByZip('a1234')
      assert.equal(
        JSON.stringify(result), JSON.stringify({
          error: 'invalid zipcode!'
        })
      )
    })
  })
})
