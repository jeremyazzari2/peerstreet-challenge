'use strict'

module.exports.getMsaForZip = (event, context, callback) => {
  const { findMsaByZip } = require('./src/find_msa.js')
  const query = event.queryStringParameters
  const zip = query.zip

  async function execute () {
    const result = await findMsaByZip(zip)
    let code = 200

    if (result.error) code = 422

    const response = {
      statusCode: code,
      body: JSON.stringify(result)
    }

    callback(null, response)
  }

  try {
    execute()
  } catch (e) {
    const error = {
      statusCode: 500,
      body: JSON.stringify({
        error: 'Something\'s not right!'
      })
    }

    callback(null, error)
  }
}
